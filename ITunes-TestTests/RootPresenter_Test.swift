//
//  RootPresenter_Test.swift
//  ITunes-TestTests
//
//  Created by Андрей Дурыманов on 16.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import XCTest
@testable import ITunes_Test

class RootPresenter_Test: XCTestCase {
    
    var rootPresenterDependency: RootPresenterDependency_mock!
    var rootPresenter: RootPresenter!

    override func setUp() {
        rootPresenterDependency = RootPresenterDependency_mock()
        rootPresenter = RootPresenter(with: rootPresenterDependency)
    }

    override func tearDown() {
        rootPresenterDependency = nil
        rootPresenter = nil
    }
    
    /// After activation root presenter should present iTunesSearch module
    func testActivation() {
        // Create mock view controller for testing
        let rootViewController = RootViewController_mock()
        // Set mock view controller as rootPresenter delegate
        rootPresenter.viewController = rootViewController
        
        rootPresenter.activate()
        XCTAssertEqual(rootViewController.presentITunesSearch, 1)
    }

}
