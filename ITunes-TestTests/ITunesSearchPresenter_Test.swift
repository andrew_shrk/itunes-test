//
//  ITunesSearchPresenter_Test.swift
//  ITunes-TestTests
//
//  Created by Андрей Дурыманов on 16.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import XCTest
@testable import ITunes_Test

class ITunesSearchPresenter_Test: XCTestCase {
    
    var dataSource_mock: DataSource_mock!
    var iTunesSearchDependency_mock: ITunesSearchDependency_mock!
    var iTunesSearchPresenter: ITunesSearchPresenter!
    var iTunesSearchViewController_mock: ITunesSearchViewController_mock!

    override func setUp() {
        dataSource_mock = DataSource_mock()
        iTunesSearchViewController_mock = ITunesSearchViewController_mock()
        iTunesSearchDependency_mock = ITunesSearchDependency_mock(dataSource: dataSource_mock)
        iTunesSearchPresenter = ITunesSearchPresenter(args: iTunesSearchDependency_mock)
        iTunesSearchPresenter.viewController = iTunesSearchViewController_mock
    }

    override func tearDown() {
        dataSource_mock = nil
        iTunesSearchDependency_mock = nil
        iTunesSearchPresenter = nil
        iTunesSearchViewController_mock = nil
    }
    
    func testSearchWithEmptyResult() {
        let searchExpectation = self.expectation(description: "Scaling")
        dataSource_mock.resetCounters()
        iTunesSearchViewController_mock.resetCounters()
        dataSource_mock.searchExpectation = searchExpectation
        dataSource_mock.searchResult = ITunesSearchResult_dummy.empty()
        dataSource_mock.searchError = nil
        iTunesSearchPresenter.didStartSearch(with: "Test")
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(dataSource_mock.iTunesSearchCall, 1)
        XCTAssertEqual(iTunesSearchViewController_mock.showActivityIndicatorCount, 1)
        XCTAssertEqual(iTunesSearchViewController_mock.hideActivityIndicatorCount, 1)
        XCTAssertEqual(iTunesSearchViewController_mock.showSearchResultCount, 1)
        XCTAssertEqual(iTunesSearchViewController_mock.resetSearchResultCount, 1)
    }
    
    func testSearchWithResult() {
        let searchExpectation = self.expectation(description: "Scaling")
        let downloadImageExpectation = self.expectation(description: "Haha")
        let imageCount = 10
        dataSource_mock.searchExpectation = searchExpectation
        dataSource_mock.downloadImageExpectation = downloadImageExpectation
        dataSource_mock.resetCounters()
        dataSource_mock.estimateDownloadImageCount = 10
        iTunesSearchViewController_mock.resetCounters()
        dataSource_mock.searchResult = ITunesSearchResult_dummy.withImages(count: imageCount)
        dataSource_mock.searchError = nil
        iTunesSearchPresenter.didStartSearch(with: "Test")
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(dataSource_mock.iTunesSearchCall, 1)
        XCTAssertEqual(dataSource_mock.downloadImageWithUrlCall, imageCount)
        XCTAssertEqual(iTunesSearchViewController_mock.showActivityIndicatorCount, 1)
        XCTAssertEqual(iTunesSearchViewController_mock.hideActivityIndicatorCount, 1)
        XCTAssertEqual(iTunesSearchViewController_mock.showSearchResultCount, 1)
        XCTAssertEqual(iTunesSearchViewController_mock.resetSearchResultCount, 1)
    }

}
