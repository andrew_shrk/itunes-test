//
//  ITunesSearchResultItem_dummy.swift
//  ITunes-TestTests
//
//  Created by Андрей Дурыманов on 16.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation
@testable import ITunes_Test

class ITunesSearchResultItem_dummy {
    
    class func albumWithImage() -> Album {
        guard let url = imageUrl else {
            fatalError("Wrong image url")
        }
        return Album(artworkUrl100: url)
    }
    
    class func artist() -> Artist {
        return Artist()
    }
    
    class func trackWithImage() -> Track {
        guard let url = imageUrl else {
            fatalError("Wrong image url")
        }
        return Track(artworkUrl100: url)
    }
    
    private static var imageUrl = URL(string: "https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg")
    
}
