//
//  ITunesSearchResult_dummy.swift
//  ITunes-TestTests
//
//  Created by Андрей Дурыманов on 16.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation
@testable import ITunes_Test

class ITunesSearchResult_dummy {
    
    class func empty() -> ITunesSearchResult {
        return ITunesSearchResult(resultCount: 0, results: [])
    }
    
    class func withItems(_ items: [ITunesSearchResultItem]) -> ITunesSearchResult {
        return ITunesSearchResult(resultCount: items.count, results: items)
    }
    
    class func withImages(count: Int) -> ITunesSearchResult {
        var items: [ITunesSearchResultItem] = []
        for i in 0..<count {
            switch i % 2 {
            case 0:
                let item = ITunesSearchResultItem_dummy.albumWithImage()
                items.append(ITunesSearchResultItem.album(item))
            default:
                let item = ITunesSearchResultItem_dummy.trackWithImage()
                items.append(ITunesSearchResultItem.track(item))
            }
        }
        return ITunesSearchResult(resultCount: items.count, results: items)
    }
    
}
