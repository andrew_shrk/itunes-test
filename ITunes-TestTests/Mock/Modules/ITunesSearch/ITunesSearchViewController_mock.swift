//
//  ITunesSearchViewController_mock.swift
//  ITunes-TestTests
//
//  Created by Андрей Дурыманов on 16.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation
@testable import ITunes_Test

class ITunesSearchViewController_mock: ITunesSearchViewControllable {
    
    var presentErrorCount = 0
    var showActivityIndicatorCount = 0
    var hideActivityIndicatorCount = 0
    var showSearchResultCount = 0
    var resetSearchResultCount = 0
    
    func resetCounters() {
        presentErrorCount = 0
        showActivityIndicatorCount = 0
        hideActivityIndicatorCount = 0
        showSearchResultCount = 0
    }
    
    func showError(_ error: Error) {
        presentErrorCount += 1
    }
    
    func showActivityIndicator() {
        showActivityIndicatorCount += 1
    }
    
    func hideActivityIndicator() {
        hideActivityIndicatorCount += 1
    }
    
    func showSearchResult(_ result: [ITunesSearchResultItem]) {
        showSearchResultCount += 1
    }
    
    func resetSearchResult() {
        resetSearchResultCount += 1
    }

}
