//
//  RootPresenterDependency_mock.swift
//  ITunes-TestTests
//
//  Created by Андрей Дурыманов on 16.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation

@testable import ITunes_Test

class RootPresenterDependency_mock: RootPresenterDependency {
    var dataService: DataSource {
        DataSource_mock()
    }
}
