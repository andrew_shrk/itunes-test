//
//  RootViewController_mock.swift
//  ITunes-TestTests
//
//  Created by Андрей Дурыманов on 16.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation
@testable import ITunes_Test

class RootViewController_mock: RootViewControllable {
    
    var presentITunesSearch = 0
    
    func presentITunesSearch(with args: ITunesSearchDependency) {
        presentITunesSearch += 1
    }

}
