//
//  DataSource_mock.swift
//  ITunes-TestTests
//
//  Created by Андрей Дурыманов on 16.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation
import XCTest
@testable import ITunes_Test

class DataSource_mock: DataSource {
    
    var iTunesSearchCall = 0
    var downloadImageWithUrlCall = 0
    
    var searchResult: ITunesSearchResult?
    var searchError: Error?
    
    var downloadImageResult: Data?
    var downloadImageError: Error?
    
    var searchExpectation: XCTestExpectation?
    var downloadImageExpectation: XCTestExpectation?
    
    var estimateDownloadImageCount = 1
    
    func resetCounters() {
        iTunesSearchCall = 0
        downloadImageWithUrlCall = 0
    }
    
    func iTunesSearch(withQuery query: String, completion: @escaping iTunesSearchCompletion) -> SessionTask {
        let task = SessionTask_mock()
        DispatchQueue.main.async { [unowned self] in
            self.iTunesSearchCall += 1
            self.searchExpectation?.fulfill()
            task.stateValue = .completed
            completion(self.searchResult, self.searchError)
        }
        return task
    }
    
    func downloadImage(withUrl url: URL, completion: @escaping DownloadImageCompletion) -> SessionTask {
        let task = SessionTask_mock()
        DispatchQueue.main.async { [unowned self] in
            self.downloadImageWithUrlCall += 1
            if self.estimateDownloadImageCount == self.downloadImageWithUrlCall {
                self.downloadImageExpectation?.fulfill()
            }
            task.stateValue = .completed
            completion(self.downloadImageResult, self.downloadImageError)
        }
        return task
    }
    
}
