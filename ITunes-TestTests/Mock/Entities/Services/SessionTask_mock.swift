//
//  SessionTask_mock.swift
//  ITunes-TestTests
//
//  Created by Андрей Дурыманов on 16.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation

@testable import ITunes_Test

class SessionTask_mock: SessionTask {
    
    var cancelCall = 0
    var resumeCall = 0
    
    var progressValue = Progress(totalUnitCount: 50)
    var stateValue = URLSessionTask.State.running
    
    func cancel() {
        cancelCall += 1
    }
    
    func resume() {
        resumeCall += 1
    }
    
    var progress: Progress {
        progressValue
    }
    
    var state: URLSessionTask.State {
        stateValue
    }
    
}
