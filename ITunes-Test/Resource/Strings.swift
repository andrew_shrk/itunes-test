//
//  Strings.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 15.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation

enum Strings: String {
    
    // MARK: Screen titles
    
    case iTunesSearchScreenTitle = "Search"
    
    // MARK: Sections
    
    case albumsSection = "Albums"
    case tracksSection = "Tracks"
    case artistsSection = "Artists"
    
    case errorTitle = "Error"
    case errorCloseActionTitle = "Close"
    
    func localized() -> String {
        let string = self.rawValue
        return NSLocalizedString(string, comment: "")
    }
    
}

enum ReusableIdentifiers: String {
    
    case iTunesSearchResultCell = "itunes_search_result_cell"
    
}
