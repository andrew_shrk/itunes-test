//
//  AppDataSource.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 15.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation

class AppDataSource: DataSource {
    
    private var endpoint: String
    private var httpService: HttpService
    
    init(httpService: HttpService, endpoint: String) {
        self.httpService = httpService
        self.endpoint = endpoint
    }
    
    enum Path: String {
        case search
        
        func url(with endpoint: String) -> String {
            endpoint + self.rawValue + "/"
        }
    }
    
    enum Key: String {
        case term
        case media
        case entity
        
        var name: String {
            rawValue
        }
    }
    
    func iTunesSearch(withQuery query: String,
                      completion: @escaping iTunesSearchCompletion) -> SessionTask {
        // Make request url
        let url = Path.search.url(with: endpoint)
        // Prepare query
        let formattingQuery = query.split(separator: " ").joined(separator: "+")
        // Make request parameters
        var parameters: [String: String] = [:]
        parameters[Key.term.name] = formattingQuery
        parameters[Key.media.name] = "music"
        parameters[Key.entity.name] = "musicArtist,album,song"
        // Send request
        do {
            return try httpService.sendRequest(url,
                                               parameters: parameters,
                                               completion: completion)
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    func downloadImage(withUrl url: URL, completion: @escaping DownloadImageCompletion) -> SessionTask {
        do {
            return try httpService.getData(url, completion: completion)
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
}
