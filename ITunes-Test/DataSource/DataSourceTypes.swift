//
//  DataSourceTypes.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 16.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation

struct ITunesSearchResult: Decodable {
    var resultCount: Int
    var results: [ITunesSearchResultItem]
}

enum ITunesSearchResultItem {
    case track(Track)
    case album(Album)
    case artist(Artist)
    case unsupported
}

extension ITunesSearchResultItem {
    enum WrapperType: String {
        case track = "track"
        case album = "collection"
        case artist = "artist"
    }
}

extension ITunesSearchResultItem: Decodable {
    
    enum CodingKeys: CodingKey {
        case wrapperType
        case results
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let wrapperType = try container.decode(String.self, forKey: .wrapperType)
        switch wrapperType {
        case WrapperType.track.rawValue:
            let item = try Track(from: decoder)
            self = .track(item)
        case WrapperType.album.rawValue:
            let item = try Album(from: decoder)
            self = .album(item)
        case WrapperType.artist.rawValue:
            let item = try Artist(from: decoder)
            self = .artist(item)
        default:
            self = .unsupported
            break
        }
    }
    
}

protocol SessionTask: class {
    func cancel()
    func resume()
    var progress: Progress { get }
    var state: URLSessionTask.State { get }
}
