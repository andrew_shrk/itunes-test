//
//  DataSource.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 12.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation

protocol DataSource {
    
    typealias iTunesSearchCompletion = (ITunesSearchResult?, Error?) -> Void
    
    typealias DownloadImageCompletion = (Data?, Error?) -> Void
    
    func iTunesSearch(withQuery query: String,
                      completion: @escaping iTunesSearchCompletion) -> SessionTask
    
    func downloadImage(withUrl url: URL,
                       completion: @escaping DownloadImageCompletion) -> SessionTask
    
}
