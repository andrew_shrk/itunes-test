//
//  Track.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 13.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation

struct Track: Codable {
    var artistId: Int?
    var collectionId: Int?
    var trackId: Int?
    var artistName: String?
    var collectionName: String?
    var trackName: String?
    var previewUrl: URL?
    var artworkUrl60: URL?
    var artworkUrl30: URL?
    var artworkUrl100: URL?
    var primaryGenreName: String?
}
