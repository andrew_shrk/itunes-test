//
//  Artist.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 13.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation

struct Artist: Codable {
    var artistName: String?
    var artistId: Int?
    var primaryGenreName: String?
    var primaryGenreId: Int?
}
