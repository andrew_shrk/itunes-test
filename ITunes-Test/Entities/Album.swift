//
//  Album.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 13.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation

struct Album: Codable {
    var artistId: Int?
    var collectionId: Int?
    var artistName: String?
    var collectionName: String?
    var artworkUrl60: URL?
    var artworkUrl100: URL?
    var trackCount: Int?
}
