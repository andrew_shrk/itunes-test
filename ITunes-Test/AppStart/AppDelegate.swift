//
//  AppDelegate.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 12.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    public var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        
        // Prepare environment
        let httpService = HttpService()
        let iTunesEndpoint = "https://itunes.apple.com/"
        let iTunesDataSource = AppDataSource(httpService: httpService, endpoint: iTunesEndpoint)
        
        // Prepare root module
        let rootPresenterArgs = RootPresenterArgs(dataService: iTunesDataSource)
        let rootPresenter = RootPresenter(with: rootPresenterArgs)
        let rootViewController = RootViewController(presenter: rootPresenter)
        
        // Launch app with root module
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
        
        return true
    }

}

