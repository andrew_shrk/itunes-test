//
//  RootPresenter.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 14.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation

protocol RootViewControllable: ViewControllable {
    // Routing
    /// Navigate to iTunes search screen
    /// - Parameter args: iTunesSearch module dependency
    func presentITunesSearch(with args: ITunesSearchDependency)
}

protocol RootPresenterDependency {
    var dataService: DataSource { get }
}

class RootPresenter: Presenter {
    
    weak var viewController: RootViewControllable?
    
    init(with args: RootPresenterDependency) {
        self.args = args
    }
    
    override func didBecomeActive() {
        // Navigate to ITunesSearch module
        let args = ITunesSearchArgs(dataSource: self.args.dataService)
        viewController?.presentITunesSearch(with: args)
    }
    
    // MARK: Private
    
    private var args: RootPresenterDependency
    
}

struct RootPresenterArgs: RootPresenterDependency {
    var dataService: DataSource
}
