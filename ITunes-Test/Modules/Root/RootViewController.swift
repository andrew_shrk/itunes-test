//
//  RootViewController.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 14.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import UIKit

class RootViewController: UINavigationController, RootViewControllable {
    
    private var presenter: RootPresenter
    
    // MARK: Inits / Deinits
    
    init(presenter: RootPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
        presenter.viewController = self
        presenter.activate()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        presenter.deactivate()
    }
    
    // MARK: ViewControllable
    
    func presentITunesSearch(with args: ITunesSearchDependency) {
        let iTunesSearchPresenter = ITunesSearchPresenter(args: args)
        let iTunesSearchViewController = ITunesSearchViewController(style: .grouped, presenter: iTunesSearchPresenter)
        show(iTunesSearchViewController, sender: self)
    }
    
}
