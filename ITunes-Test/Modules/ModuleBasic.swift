//
//  ModuleBasic.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 16.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation

/// Протокол описывает контракт между Presenter и ViewController
protocol ViewControllable: class {}

/// Base class for Presenters
///
/// Отвечает за связь ViewController и Model
open class Presenter: NSObject {
    
    func activate() {
        didBecomeActive()
    }
    
    func deactivate() {
        willResignActive()
    }
    
    internal func didBecomeActive() {}
    
    internal func willResignActive() {}
    
}
