//
//  ITunesSearchViewController.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 12.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import UIKit

protocol ITunesSearchPresentable {
    
    /// Notify presenter about start search
    /// - Parameter query: Query for search
    func didStartSearch(with query: String?)
    
    /// Notify presenter about cancel search
    func didCancelSearch()
    
    /// Request image preloaded image data for URL
    /// - Parameter url: Image download url
    func imageData(for url: URL?) -> Data?
}

class ITunesSearchViewController: UITableViewController, ITunesSearchViewControllable {
    
    private var presenter: ITunesSearchPresenter
    
    // MARK: Inits / Deinits
    
    init(style: UITableView.Style, presenter: ITunesSearchPresenter) {
        self.presenter = presenter
        super.init(style: style)
        presenter.viewController = self
        presenter.activate()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
       presenter.deactivate()
    }
    
    // MARK: UIViewController override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareTableView()
        self.prepareSearchControl()
        self.prepareTitle()
    }
    
    // MARK: - ViewControllable
    
    func showError(_ error: Error) {
        let message = error.localizedDescription
        let cancelAction = UIAlertAction(title: Strings.errorCloseActionTitle.localized(),
                                         style: .cancel,
                                         handler: nil)
        let alert = UIAlertController(title: Strings.errorTitle.localized(),
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    func showActivityIndicator() {
        if progressHUD == nil {
            let progressHUD = ProgressHUD()
            view.addSubview(progressHUD)
            self.progressHUD = progressHUD
        }
        progressHUD?.show()
    }
    
    func hideActivityIndicator() {
        progressHUD?.hide()
    }
    
    func showSearchResult(_ result: [ITunesSearchResultItem]) {
        resetSectionsValues()
        for item in result {
            switch item {
            case .artist:
                _sections[0].items.append(item)
            case .album:
                _sections[1].items.append(item)
            case .track:
                _sections[2].items.append(item)
            default:
                break
            }
        }
        tableView.reloadData()
    }
    
    func resetSearchResult() {
        resetSectionsValues()
        tableView.reloadData()
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title.localized()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reusableIdentifier, for: indexPath)
        let section = indexPath.section
        let row = indexPath.row
        let item = sections[section].items[row]
        var title: String?
        var subTitle: String?
        var imageUrl: URL?
        switch item {
        case .artist(let artist):
            title = artist.artistName
            subTitle = artist.primaryGenreName
        case .album(let album):
            imageUrl = album.artworkUrl100
            title = album.collectionName
            subTitle = album.artistName
        case .track(let track):
            title = track.trackName
            subTitle = track.artistName
            imageUrl = track.artworkUrl100
        default:
            break
        }
        setupCell(cell, title: title, subTitle: subTitle, imageUrl: imageUrl)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Private
    
    private var progressHUD: ProgressHUD?
    
    private var _sections = [
        Section<ITunesSearchResultItem>(title: .artistsSection, items: []),
        Section<ITunesSearchResultItem>(title: .albumsSection, items: []),
        Section<ITunesSearchResultItem>(title: .tracksSection, items: [])
    ]
    
    private var sections: [Section<ITunesSearchResultItem>] {
        _sections.filter { !$0.items.isEmpty }
    }
    
    private let reusableIdentifier = ReusableIdentifiers.iTunesSearchResultCell.rawValue
    
    private func prepareTableView() {
        tableView.register(ITunesSearchResultCell.self, forCellReuseIdentifier: reusableIdentifier)
    }
    
    private func prepareSearchControl() {
        let searchControl = UISearchController()
        searchControl.searchBar.delegate = self
        searchControl.dimsBackgroundDuringPresentation = false
        navigationItem.searchController = searchControl
    }
    
    private func prepareTitle() {
        title = Strings.iTunesSearchScreenTitle.localized()
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    // MARK: Helpers
    
    private func setupCell(_ cell: UITableViewCell, title: String?, subTitle: String?, imageUrl: URL?) {
        if let imageData = presenter.imageData(for: imageUrl) {
            let image = UIImage(data: imageData)
            cell.imageView?.image = image
        } else {
            cell.imageView?.image = nil
        }
        cell.textLabel?.text = title
        cell.detailTextLabel?.text = subTitle
    }
    
    private func resetSectionsValues() {
        for (key, _) in _sections.enumerated() {
            _sections[key].items = []
        }
    }
    
}

extension ITunesSearchViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let query = searchBar.text
        presenter.didStartSearch(with: query)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        presenter.didCancelSearch()
    }
    
}

struct Section<T> {
    var title: Strings
    var items: [T]
}
