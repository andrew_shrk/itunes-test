//
//  ITunesSearchPresenter.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 14.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation

protocol ITunesSearchViewControllable: ViewControllable {
    
    /// Method present error alert to user
    /// - Parameter error: error
    func showError(_ error: Error)
    
    /// Method show activity indicator
    func showActivityIndicator()
    
    /// Method display search results to user
    /// - Parameter result: Search results
    func showSearchResult(_ result: [ITunesSearchResultItem])
    
    /// Method hide activity indicator
    func hideActivityIndicator()
    
    /// Method remove all search result views
    func resetSearchResult()
    
}

protocol ITunesSearchDependency {
    var dataSource: DataSource { get }
}

class ITunesSearchPresenter: Presenter, ITunesSearchPresentable {
    
    weak var viewController: ITunesSearchViewControllable?
    
    init(args: ITunesSearchDependency) {
        self.args = args
    }
    
    override func willResignActive() {
        cancelAllTasks()
    }
    
    // MARK: Presentable
    
    func didStartSearch(with query: String?) {
        // Cancel previously tasks
        cancelAllTasks()
        // Clear images data
        clearImagesData()
        // Clear old results
        viewController?.resetSearchResult()
        if let query = query {
            startSearchTask(with: query)
        }
    }
    
    func didCancelSearch() {
        cancelAllTasks()
        clearImagesData()
        viewController?.resetSearchResult()
        viewController?.hideActivityIndicator()
    }
    
    func imageData(for url: URL?) -> Data? {
        guard let url = url else { return nil }
        return imagesData[url]
    }
    
    // MARK: - Private
    
    private let args: ITunesSearchDependency
    
    private var searchTask: SessionTask?
    
    private var imagesDownloadTasks: [SessionTask] = []
    
    private var imagesData: [URL: Data] = [:]
    
    private var searchResults: [ITunesSearchResultItem] = []
    
    // MARK: Server tasks
    
    private func startSearchTask(with query: String) {
        // Hide activity indicator in handler
        viewController?.showActivityIndicator()
        let searchTask = args
            .dataSource
            .iTunesSearch(withQuery: query, completion: { [unowned self] (result, error) in
                self.iTunesSearchHandler(result, error)
            })
        // Start task
        searchTask.resume()
        self.searchTask = searchTask
    }
    
    private func startDownloadImages(for items: [ITunesSearchResultItem]) {
        // Get urls for downloading form result items
        let urls = getImageUrls(from: items)
        let imagesDownloadTasks = urls.map { (url) -> SessionTask in
            args.dataSource.downloadImage(withUrl: url) { [unowned self] (data, error) in
                self.imageDownloadHandler(url: url, data, error: error)
            }
        }
        // Start download tasks
        imagesDownloadTasks.forEach { $0.resume() }
        self.imagesDownloadTasks = imagesDownloadTasks
    }
    
    /// Method canceling all tasks
    private func cancelAllTasks() {
        cancelSearchTask()
        cancelDownloadImageTasks()
    }
    
    private func cancelSearchTask() {
        searchTask?.cancel()
        searchTask = nil
    }
    
    private func cancelDownloadImageTasks() {
        imagesDownloadTasks.forEach { $0.cancel() }
        imagesDownloadTasks = []
    }
    
    // MARK: Handlers
    
    /// Method handle search result
    ///
    /// If needed begin download images
    private func iTunesSearchHandler(_ result: ITunesSearchResult?, _ error: Error?) {
        if let error = error {
            viewController?.showError(error)
            return
        }
        let resultItems = result?.results ?? []
        if resultItems.isEmpty {
            viewController?.showSearchResult(searchResults)
            viewController?.hideActivityIndicator()
        } else {
            startDownloadImages(for: resultItems)
        }
        self.searchResults = resultItems
    }
    
    /// Method handle image download result
    private func imageDownloadHandler(url: URL, _ imageData: Data?, error: Error?) {
        if let error = error {
            viewController?.showError(error)
            return
        }
        imagesData[url] = imageData
        if imagesDownloadTasks.filter({ $0.state == .running }).isEmpty {
            viewController?.showSearchResult(searchResults)
            viewController?.hideActivityIndicator()
        }
    }
    
    // MARK: Helpers
    
    private func getImageUrls(from items: [ITunesSearchResultItem]) -> [URL] {
        return items.compactMap { (item) -> URL? in
            switch item {
            case .album(let album):
                return album.artworkUrl100
            case .track(let track):
                return track.artworkUrl100
            default:
                return nil
            }
        }
    }
    
    private func clearImagesData() {
        imagesData = [:]
    }
    
}

struct ITunesSearchArgs: ITunesSearchDependency {
    var dataSource: DataSource
}
