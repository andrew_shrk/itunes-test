//
//  ITunesSearchResultCell.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 16.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import UIKit

class ITunesSearchResultCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

