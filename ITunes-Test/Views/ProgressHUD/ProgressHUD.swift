//
//  ProgressHUD.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 16.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import UIKit

class ProgressHUD: UIVisualEffectView {

    lazy var activityIndictor: UIActivityIndicatorView = {
        if #available(iOS 13.0, *) {
            return UIActivityIndicatorView(style: .large)
        } else {
            return UIActivityIndicatorView(style: .whiteLarge)
        }
    }()
    let blurEffect = UIBlurEffect(style: .light)
    let vibrancyView: UIVisualEffectView

    init() {
        self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
        super.init(effect: blurEffect)
        self.setup()
    }

    required init?(coder aDecoder: NSCoder) {
        self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
        super.init(coder: aDecoder)
        self.setup()
    }

    func setup() {
        contentView.addSubview(vibrancyView)
        contentView.addSubview(activityIndictor)
        activityIndictor.startAnimating()
        layer.zPosition = 100
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        if let superview = self.superview {
            self.frame = superview.bounds
            vibrancyView.frame = self.bounds
            activityIndictor.center = self.center
            layer.masksToBounds = true
        }
    }

    func show() {
        self.isHidden = false
    }

    func hide() {
        self.isHidden = true
    }
}
