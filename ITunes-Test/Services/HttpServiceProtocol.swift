//
//  HttpService.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 15.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation

/// Протокол описывает методы для работы с HTTP сервером
protocol HttpServiceProtocol {
    
    /// Send GET request
    /// - Parameters:
    ///   - url
    ///   - parameters: GET parameters
    ///   - completion
    func sendRequest<T: Decodable>(_ url: String,
                                   parameters: [String: String],
                                   completion: @escaping (T?, Error?) -> Void) throws -> SessionTask
    
    /// Download data from server
    /// - Parameters:
    ///   - url
    ///   - completion
    func getData(_ url: URL, completion: @escaping (Data? ,Error?) -> Void) throws -> SessionTask
    
}
