//
//  HttpService.swift
//  ITunes-Test
//
//  Created by Андрей Дурыманов on 15.03.2020.
//  Copyright © 2020 Durymanov Andrew. All rights reserved.
//

import Foundation

/// Реализация работы протокола для работы  HTTP сервером
class HttpService: HttpServiceProtocol {
    
    enum HttpError: Error {
        case wrongUrlFormat
    }
    
    var decoder: JSONDecoder = JSONDecoder()
    
    // MARK: - HttpServiceProtocol
    
    func sendRequest<T: Decodable>(_ url: String,
                                   parameters: [String: String],
                                   completion: @escaping (T?, Error?) -> Void) throws -> SessionTask {
        // Prepare url
        let requestUrl = try makeUrl(url, with: parameters)
        let request = URLRequest(url: requestUrl)
        // Make request
        let task = URLSession.shared.dataTask(with: request) { [unowned self] data, _, error in
            self.requestHandler(data, error: error, completion: completion)
        }
        return task
    }
    
    
    /// Method create download task and run completion after download complete
    /// - Parameters:
    ///   - url: data url
    ///   - completion: completion
    func getData(_ url: URL, completion: @escaping (Data? ,Error?) -> Void) throws -> SessionTask {
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { (data, _, error) in
            DispatchQueue.main.async {
                completion(data, error)
            }
        }
        return task
    }
    
    // MARK: - Private
    
    /// Create URL with parameters
    /// - Parameters:
    ///   - link: Base URL string
    ///   - parameters: URL parameters
    private func makeUrl(_ link: String, with parameters: [String: String]?) throws -> URL {
        guard var components = URLComponents(string: link) else {
            throw HttpError.wrongUrlFormat
        }
        components.queryItems = parameters?.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        guard let requestUrl = components.url else {
            throw HttpError.wrongUrlFormat
        }
        return requestUrl
    }
    
    /// Decode json object to swift object
    /// - Parameter data: raw json data
    private func decodeResponse<T: Decodable>(data: Data) throws -> T {
        return try decoder.decode(T.self, from: data)
    }
    
    
    /// Method parse data if request without error else run completion with error
    /// - Parameters:
    ///   - data: request data
    ///   - error: request error
    ///   - completion: completion
    private func requestHandler<T: Decodable>(_ data: Data?, error: Error?, completion: @escaping (T?, Error?) -> Void) {
        var finalError: Error? = error
        var responseObject: T?
        // Run completion on main tread after function finish execution
        defer {
            DispatchQueue.main.async  {
                completion(responseObject, finalError)
            }
        }
        // If error do nothing
        if error != nil { return }
        // Else decode data
        if let data = data {
            do { responseObject = try self.decodeResponse(data: data) }
            catch { finalError = error }
        }
    }

}

// MARK: - SessionTask
extension URLSessionTask: SessionTask {}
